import numpy as np

allcoords = np.loadtxt('element/allcoords.txt', skiprows=2)[:, 1:]
elconec   = np.loadtxt('element/elconec.txt', dtype=int)[5:] - 1
elcoords  = np.round(allcoords[elconec,:], decimals=4)
np.savetxt('element/elcoords.txt', elcoords, fmt='%.4f')