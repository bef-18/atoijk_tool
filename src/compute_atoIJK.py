import numpy as np
import sys

## Set element characteristics
ndims  = int(sys.argv[1])
order  = int(sys.argv[2])
nnods = (order+1)**ndims
atoIJK = np.zeros((1,nnods),dtype=int)

## Element to obtain the atoIJK
if ndims == 2:
    elxyz = np.loadtxt('element/elcoords.txt')[:,:2]
if ndims == 3:
    elxyz = np.loadtxt('element/elcoords.txt')

## Generate reference element
xi    = np.round(np.linspace(-1,1, num = order+1), decimals=4)
xi    = np.hstack((np.array([xi[0], xi[-1]]), xi[1:-1]))
coord = np.zeros((nnods,ndims),dtype=np.double)
if ndims == 2:
    for i in range(order+1):
        for j in range(order+1):
            coord[i*(order+1) + j, :] = np.array([xi[i], xi[j]])
if ndims == 3:
    for k in range(order+1):
        for i in range(order+1):
            for j in range(order+1):
                coord[k*(order+1)**2 + i*(order+1) + j, :] = np.array([xi[i], xi[j], xi[k]])

## Compute the atoIJK 
for inod in range(nnods):
    for jnod in range(nnods):
        if np.all(coord[inod] == elxyz[jnod]):
            atoIJK[0,inod] = jnod +1
print('The atoIJK of the element is: ', atoIJK)
np.savetxt('atoIJK.txt', atoIJK, fmt='%i')