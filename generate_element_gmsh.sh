while getopts d:o: flag
do
    case "${flag}" in
        d) dims=${OPTARG};;
        o) order=${OPTARG};;
    esac
done
echo "------Getting the atoIJK of a "$dims"D "$order" order element"
echo "------Generating element in Gmesh------"
gmsh gmsh/element.geo -3 -order $order -o gmsh/element.msh
echo "------Element generated------"
nnodes=$(grep \$Nodes -A1 gmsh/element.msh | grep -v \$Nodes)
echo "------Number of nodes: "$nnodes"------"
nlines=$(($nnodes+1))
rm element/*.txt
grep \$Nodes -A$nlines gmsh/element.msh > element/allcoords.txt
echo "------Nodal coordinates saved------"
if (($dims == 2))
then
    grep \$Elements -A27 gmsh/element.msh | tail -1 >> element/elconec.txt
fi
if (($dims == 3))
then
    grep \$Elements -A28 gmsh/element.msh | tail -1 >> element/elconec.txt
fi
python3 src/get_element_coords.py
python3 src/compute_atoIJK.py $dims $order