# Element information needed for the atoIJK computation

The three files added in this folder after executing the script are:

## alcoords.txt

The coordinates of all the nodes of the mesh.

## elconec.txt

The connectivity of the element that we want to find the atoIJK from.

## elcoords.txt

The coordinates of the nodes in the element.