// Gmsh project created on Thu Jan 12 18:34:29 2023
Mesh.MshFileVersion = 2.2;
//+
Point(1) = {-1, -1, -1, 2};
//+
Point(2) = {1, -1, -1, 2};
//+
Point(3) = {1, 1, -1, 2};
//+
Point(4) = {-1, 1, -1, 2};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Transfinite Curve {1, 2, 3, 4} = 1 Using Progression 1;
//+
Extrude {0, 0, 2} {
  Surface{1}; Layers {1}; Recombine;
}
// Settings
//Mesh.ElementOrder = 3;
Mesh.Algorithm = 6;
Mesh.Algorithm3D = 1;
Mesh.RecombinationAlgorithm = 1;
Mesh.RecombineAll = 1;
Mesh.SubdivisionAlgorithm = 1;